# ics-ans-role-zeek

Ansible role to install zeek.

## Role Variables

```yaml
zeek_random_salt: "48hvrnvjbnvuehvnvlk482yhnvjfb;vjkvbjsdvktunvk"
zeek_interface: <interface>
zeek_distribution_list: user@ess.eu,user1@ess.eu
zeek_lb_procs: 2
zeek_pin_cpus: "2,3"
zeek_restrict_filter: []
zeek_log_rotate: 86400
# min, hour day
zeek_expire_interval: "4 day"
zeek_networks:
  - name: "office network"
    subnet: "10.3.0.0/22"
  - name: "dmz network"
    subnet: "10.4.0.0/22"
zeek_packages:
  - https://github.com/mitre-attack/bzar
  - zeek-af_packet-plugin
  - zeek/hosom/file-extraction
  - zeek/j-gras/add-interfaces
  - zeek/j-gras/zeek-af_packet-plugin
  - zeek/jsiwek/zeek-cryptomining
  - zeek/mitrecnd/bro-http2
  - zeek/ncsa/bro-is-darknet
  - zeek/salesforce/hassh
  - zeek/salesforce/ja3
  - zeek/sethhall/domain-tld
zkg_bin: /opt/zeek/bin/zkg
zkg_install_script: /opt/zeek/bin/devtools.sh
...
```

## Example Playbook

```yaml
- hosts: servers
  roles:
    - role: ics-ans-role-zeek
```

## License

BSD 2-clause
---
