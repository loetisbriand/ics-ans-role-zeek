redef Notice::ignored_types+= { PacketFilter::Dropped_Packets };
redef Notice::ignored_types+= { ATTACK::Discovery };

#hook Notice::policy(n: Notice::Info)
#    {
#    add n$actions[Notice::ACTION_LOG];
#    add n$actions[Notice::ACTION_EMAIL];
#    }
hook Notice::policy(n: Notice::Info)
{
        if ( n$note == ATTACK::Execution || n$note == ATTACK::Lateral_Movement || n$note == ATTACK::Lateral_Movement_and_Execution || n$note == SSH::Password_Guessing || n$note == TeamCymruMalwareHashRegistry::Match)
    		add n$actions[Notice::ACTION_LOG];
                add n$actions[Notice::ACTION_EMAIL];
                n$suppress_for = 1hrs;
        if ( n$note == Scan::Port_Scan )
    		add n$actions[Notice::ACTION_LOG];
                add n$actions[Notice::ACTION_EMAIL];
                n$suppress_for = 1hrs;
        if ( n$note == Traceroute::Detected || n$note == Scan::Address_Scan )
    		add n$actions[Notice::ACTION_LOG];
                n$suppress_for = 1hrs;
}
